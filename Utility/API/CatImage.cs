using Newtonsoft.Json;

namespace csharpi.Utility.API
{
    public class CatImage
    {
        public string Url => File;
        public string Tags => "";
        public string SourceUrl => "";
        public string Score => "N/A";
        public string Provider => "Cat";

        [JsonProperty("file")]
        public string File { get; set; }
    }
    
    public class File
    {
        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("ext")]
        public string Ext { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("md5")]
        public string Md5 { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}