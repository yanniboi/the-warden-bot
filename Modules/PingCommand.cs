using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

namespace csharpi.Modules
{
    // for commands to be available, and have the Context passed to them, we must inherit ModuleBase
    public class PingCommands : ModuleBase
    {
        [Command("ping")]
        public async Task PingCommand()
        {
            var sb = new StringBuilder();
            sb.AppendLine("pong");
            await ReplyAsync(sb.ToString());
        }
    }
}