using System.Net;
using System.Text;
using System.Threading.Tasks;
using csharpi.Utility.API;
using Discord;
using Discord.Commands;
using Newtonsoft.Json;

namespace csharpi.Modules
{
    // for commands to be available, and have the Context passed to them, we must inherit ModuleBase
    public class RandomCommands : ModuleBase
    {
        [Command("cat")]
       
        public async Task CatCommand()
        {
            using WebClient c = new WebClient();
            byte[] b = c.DownloadData("http://aws.random.cat/meow");
            string str = Encoding.Default.GetString(b);
            CatImage cat = JsonConvert.DeserializeObject<CatImage>(str);
            
            var embed = new EmbedBuilder();
            embed.Title = "🐱 Kitties!";
            embed.Color = new Color(0.8f, 0.6f, 0.4f);
            embed.ImageUrl = cat.File;
            await ReplyAsync(null, false, embed.Build());
        }
    }
}